################################
# for run you shoud execute    #
#                              #
# -----------------------------#
# sh env.sh                    #
# -----------------------------#
################################

apt-get -y install \
  apt-transport-https \
  ca-certificates \
  curl

################################
#                              #
# adds key                     #
#                              #
################################
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

sudo add-apt-repository \
       "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
       $(lsb_release -cs) \
       stable"

sudo apt-get update

apt-get -y install docker-ce

docker run hello-world

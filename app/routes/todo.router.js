module.exports.inject = function (todos) {
  const router = require('express').Router();

  let nextId = 4;

  router.get('/api/todos', function (req, res, next) {
    res.status(200).json(todos);
  });

  router.post('/api/todos', function (req, res, next) {
    const todo = {
      id: nextId++,
      title: req.body.title,
      completed: false
    };

    todos.push(todo);

    res.status(200).json(todo);
  });

  router.put('/api/todos/:id', function (req, res, next) {
    const todo = todos.find(todo => todo.id == req.params.id);

    if (!todo) return res.sendStatus(404);

    todo.title = req.body.title || todo.title;

    res.json(todo);
  });

  router.patch('/api/todos/:id', function (req, res, next) {
    const todo = todos.find(todo => todo.id == req.params.id);

    if (!todo) return res.sendStatus(404);

    todo.completed = !todo.completed;

    res.json(todo);
  });

  router.delete('/api/todos/:id', (req, res) => {
    const index = todos.findIndex(todo => todo.id == req.params.id);

    if (index === -1) return res.sendStatus(404);

    todos.splice(index, 1);

    res.sendStatus(204);
  });

  return router;
};

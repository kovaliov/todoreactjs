import React from 'react';
import ReactDOM from 'react-dom';
import axios from 'axios';
import ReactCSSTransitionGroup from 'react-addons-css-transition-group';

import todos from './todos';

import Header from './components/header.component';
import Todo from './components/todo.component';
import Form from './components/form.component';

class App extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      todos: []
    };

    this.handleStatusChange = this.handleStatusChange.bind(this);
    this.handleDelete = this.handleDelete.bind(this);
    this.handleAdd = this.handleAdd.bind(this);
    this.handleEdit = this.handleEdit.bind(this);
  }

  handleStatusChange(id) {
    axios.patch(`/api/todos/${id}`)
      .then((res) => {
        const todos = this.state.todos.map(todo => {
          if (todo.id === id) {
            todo = res.data;
          }

          return todo;
        });

        this.setState({ todos });
      })
      .catch(this.handleError);
  }

  handleDelete(id) {
    console.log(id);
    axios.delete(`/api/todos/${id}`)
      .then(() => {
        const todos = this.state.todos.filter(todo => todo.id !== id);

        console.log(todos);
        this.setState({ todos });
      })
      .catch(this.handleError);
  }

  handleAdd(title) {
    axios.post('/api/todos', { title })
      .then((res) => {
        return res.data;
      })
      .then((todo) => {
        const todos = [...this.state.todos, todo];

        this.setState({ todos });
      })
      .catch(this.handleError);
  }

  handleEdit(id, title) {
    axios.put(`/api/todos/${id}`, { title })
      .then((res) => {
        const todos = this.state.todos.map(todo => {
          if (todo.id === id) {
            todo = res.data;
          }

          return todo;
        });

        this.setState({ todos });
      })
      .catch(this.handleError);
  }

  handleError(err) {
    console.log(err);
  }

  nextId() {
    this._nextId = this._nextId || 4;

    return this._nextId++;
  }

  componentDidMount() {
    fetch('/api/todos')
      .then((response) => {
        return response.json()
      })
      .then((todos) => {
        this.setState({ todos });
      })
      .catch(this.handleError);
  }

  render() {
    return (
      <div className="main">

        <Header title={this.props.title} todos={this.state.todos} />

        <ReactCSSTransitionGroup
          className="section todo-list"
          transitionName="slide"
          transitionEnter={true}
          transitionLeave={true}
          transitionAppear={true}
          transitionAppearTimeout={1000}
          transitionEnterTimeout={1000}
          transitionLeaveTimeout={1000} >

          {
            this.state.todos.map((todo) => {
              return <Todo key={todo.id}
                           id={todo.id}
                           title={todo.title}
                           completed={todo.completed}
                           onStatusCnahge={this.handleStatusChange}
                           onDelete={this.handleDelete}
                           onEdit={this.handleEdit} />
            })
          }

        </ReactCSSTransitionGroup>


        <Form onAdd={this.handleAdd} />

      </div>
    );
  }
}

App.propsTypes = {
  title: React.PropTypes.string.isRequired,
  initialData: React.PropTypes.arrayOf(React.PropTypes.shape({
    id: React.PropTypes.number.isRequired,
    title: React.PropTypes.string.isRequired,
    completed: React.PropTypes.bool.isRequired
  })).isRequired
};

ReactDOM.render(<App initialData={todos} title="React Todo" />, document.querySelector('#root'));

# README #

### Heroku 

```
https://warm-springs-56235.herokuapp.com/
```

The purpose of this project is demonstrate knowledge in ReactJS.  

### Success criteria ###

* JSX
* VDOM
* Dev Tools for React
* Props in component
* Validation Props value
* Composition
* States
* Change states
* Syntax of components
* Event in ReactJS
* Stateless component
* Life-cycles of component
* Fetch 
* Axios
* ReactCSSTransitionGroup

### Docker 

Build images:
```
docker build -t vvkovaliov/docker-react .
```

Start builded image on port 49160:
```
docker run -p 49160:3000 -d vvkovaliov/docker-react
```

Test on your local machine:
```
curl -i localhost:49160
```